r-VEX HDL sources
=================

This repository contains the HDL sources and source generators for the r-VEX.


Configuration and build process
-------------------------------

The many configuration options of the r-VEX are used in several phases of the
build process, and as such are inevitably stored and used in different places.
The process is roughly as follows:

 - Optional: modify the files in `config` to your liking. This allows you to
   modify the pipeline, control registers, instruction set, and trap IDs.
   However, this is for power users only, as you'll manually have to modify the
   rest of the toolchain as well for things to work together.
 - `rvex-cfg.py <json>`. Configures some common options by generating some
   configuration files and setting configuration value defaults for later
   stages. This is automatically run by the r-VEX toolchain generator, in which
   case the JSON file is generated based on a user-friendly INI file. If you're
   building manually, you can run `rvex-cfg.py -` instead to generate the
   required files.
 - `configure`. Generates a Makefile for the later steps. Allows you to set a
   custom build directory (by running the `configure` command from another
   working directory) and a custom installation directory (defaults to
   `install`).
 - `make`. Runs the code generation stage, which generates some r-VEX VHDL
   files and some header files.
 - `make install`. Copies the generated files to the specified `install`
   directory.

This generates VHDL files for you to include in a larger system, like an IP
block. Many r-VEX configuration options are VHDL generics, so at the
instantiation stage you can still change a lot of things. This allows you to
make, for instance, heterogeneous multicore platforms with only one set of
r-VEX VHDL files.

However, usually you'll want to generate a different toolchain for each core.
You can often get away with using generic binaries instead, but this will cost
performance, so if you're going to compile different programs for the different
cores anyway, you won't want to do that. Therefore, we made a toolchain
generator to simplify the process of matching the toolchain to a core. For as
far as the toolchain generator is concerned, the r-VEX HDL sources are just
another tool, which it automatically configures using the
`rvex-cfg.py` - `configure` - `make` - `make install` recipe. When you use this
method, the toolchain generator controls the *default* values for the generics,
so in this case you probably won't want to set them manually when instantiating
the cores. In fact, the same thing goes for most tools in the toolchain, except
instead of generics it's command line flags.


User manual generation
----------------------

During the code generation phase (`make`), the Makefile will also attempt to
generate the user manual PDF for the core. This requires an installation of
`pdflatex` as well as a number of `.sty` packages. Because this is rather
tedious to get working, the Makefile will ignore errors during the PDF build.
Errors are logged in `[build/]doc-out/latex-log` and
`[install/]rvex-elf32/doc/latex-log` if you want to debug, or you can manually
rerun the Makefile in `[build/]doc-build/src/userman` after running the master
Makefile once to debug the build.

If the PDF build fails, you simply won't get an up-to-date user manual. However,
an old version is included as an appendix in `rvex-thesis.pdf`, which is good
enough for most purposes.

