#!/usr/bin/env python3

import sys
import json

if __name__ == '__main__':
    
    # Check command line/print usage.
    if len(sys.argv) < 2:
        print('Usage: %s <cfg.json>' % sys.argv[0], file=sys.stderr)
        print('', file=sys.stderr)
        print('Configures the source tree in the current directory according to the given', file=sys.stderr)
        print('configuration file. Returns 0 if successful, 3 if the tool should not be', file=sys.stderr)
        print('installed, or something else (usually 1) if an error occurred.', file=sys.stderr)
        print('', file=sys.stderr)
        print('You can also run `%s -` to have the script generate default versions of the', file=sys.stderr)
        print('source files. These are the ones that should be checked into the git repos.', file=sys.stderr)
        sys.exit(2)
    
    # Load the configuration file.
    if sys.argv[1].strip() == '-':
        cfg_json = {}
    else:
        with open(sys.argv[1], 'r') as f:
            cfg_json = json.load(f)

class Config(object):
    
    def __init__(self, d, prefix=''):
        self._d = d
        self._p = prefix
        self._s = set()
        for key in d:
            if not key.startswith(prefix):
                continue
            key = key[len(prefix):].split('.')
            if len(key) == 1:
                continue
            self._s.add(key[0])
    
    def __getattr__(self, name):
        val = self._d.get(self._p + name, None)
        if val is not None:
            return val
        
        if name in self._s:
            return Config(self._d, self._p + name + '.')
        
        raise AttributeError(name)
    
    def __str__(self):
        data = ['    %s: %s\n' % (key[len(self._p):], value) for key, value in sorted(self._d.items()) if key.startswith(self._p)]
        return 'Config(\n' + ''.join(data) + ')'

def parse_cfg(cfg_json, defaults):
    """Parses the config.json contents into a Config object that is guaranteed
    to contain the keys in `defaults`. An error is printed if the given cfg_json
    dict contains non-default keys which are not present in the `defaults`
    dict, which implies that the configuration file has a higher version than
    this file and is doing something that isn't backwards compatible."""
    
    # Check that we don't have any "required" keys that we don't know about.
    for key, value in cfg_json.items():
        if value['req'] and key not in defaults:
            print('Error: found non-default key "%s", which this version of rvex-cfg.py doesn\'t know about!' % key, file=sys.stderr)
            sys.exit(1)
    
    # Build our cfg dictionary.
    cfg = {}
    for key, default in defaults.items():
        value = cfg_json.get(key, {'val': default})
        if value is None:
            print('Error: no configuration value specified for key "%s"!' % key, file=sys.stderr)
            sys.exit(1)
        cfg[key] = value['val']
    
    return Config(cfg)

def template(infname, outfname, *args, **kwargs):
    """Template engine simply using Python's str.format()."""
    with open(infname, 'r') as inf:
        with open(outfname, 'w') as outf:
            outf.write(inf.read().format(*args, **kwargs))

def template2(infname, outfname, *args, **kwargs):
    """Template engine using Python's str.format(), but with different
    characters that are more suitable for C:
     - open:   <|
     - close:  |>
     - escape: <-| to <|
               |-> to |>
    """
    with open(infname, 'r') as inf:
        with open(outfname, 'w') as outf:
            data = inf.read()
            data = data.replace('{', '{{').replace('}', '}}')
            data = data.replace('<|', '{').replace('|>', '}')
            data = data.format(*args, **kwargs)
            data = data.replace('<-|', '<|').replace('|->', '|>')
            outf.write(data)

def dont_build(s=''):
    if s:
        s = ': ' + s
    print('This tool does not need to be built%s' % s, file=sys.stderr)
    sys.exit(3)

def misconfigured(s):
    if s:
        s = ': ' + s
    print('Configuration error%s' % s, file=sys.stderr)
    sys.exit(1)

def done(s):
    if s:
        s = ': ' + s
    print('Configuration complete%s' % s, file=sys.stderr)
    sys.exit(0)

#===============================================================================
# Tool-specific from here onwards
#===============================================================================

cfg = parse_cfg(cfg_json, {
    
    # Configuration options that control the defaults of the CFG generic.
    'resources.num_lanes':        8,
    'resources.num_groups':       4,
    'resources.num_contexts':     4,
    'resources.bundle_align':     2,
    'resources.limmh_prev':       False,
    'resources.limmh_neighbor':   True,
    'resources.int_mul_lanes':    [True] * 8,
    'resources.float_add_lanes':  [False] * 8,
    'resources.float_mul_lanes':  [False] * 8,
    'resources.float_cmp_lanes':  [False] * 8,
    'resources.float_i2f_lanes':  [False] * 8,
    'resources.float_f2i_lanes':  [False] * 8,
    'resources.mem_lane':         0,
    'memory.insn_base_rvex':      0,
    'memory.ctrl_base_rvex':      0xFFFFFC00,
    'debug.num_breakpoints':      4,
    'debug.trace_unit':           False,
    'debug.perf_cnt_width':       4,
    'special.disable_forward':    False,
    'special.disable_traps':      False,
    
    # Configuration option that selects between the pipeline definitions.
    'special.extend_pipeline':    False,
    
})

# Configure core_pkd.vhd.
data = {}
try:
    data['num_lanes_log2'] = {2:1, 4:2, 8:3, 16:4}[cfg.resources.num_lanes]
    data['num_groups_log2'] = {1:0, 2:1, 4:2, 8:3}[cfg.resources.num_groups]
    data['num_contexts_log2'] = {1:0, 2:1, 4:2, 8:3}[cfg.resources.num_contexts]
    data['bundle_align_log2'] = {2:1, 4:2, 8:3, 16:4}[cfg.resources.bundle_align]
except KeyError:
    misconfigured('illegal value for "resources.num_lanes", "resources.num_groups", "resources.num_contexts", or "resources.bundle_align"')

for name in ['int_mul', 'float_add', 'float_mul', 'float_cmp', 'float_i2f', 'float_f2i']:
    data[name + '_lanes'] = ''.join(['1' if x else '0' for x in reversed(getattr(cfg.resources, name + '_lanes'))])

data['mem_lane_rev'] = str(cfg.resources.num_lanes // cfg.resources.num_groups - cfg.resources.mem_lane - 1)
data['num_breakpoints'] = str(cfg.debug.num_breakpoints)
data['forwarding'] = 'false' if cfg.special.disable_forward else 'true'
data['traps'] = '0' if cfg.special.disable_traps else '2'
data['limmh_neighbor'] = 'true' if cfg.resources.limmh_neighbor else 'false'
data['limmh_prev'] = 'true' if cfg.resources.limmh_prev else 'false'
data['ctrl_base_rvex'] = '%08X' % cfg.memory.ctrl_base_rvex
data['insn_base_rvex'] = '%08X' % cfg.memory.insn_base_rvex
data['trace_unit'] = 'true' if cfg.debug.trace_unit else 'false'
data['perf_cnt_width'] = str(cfg.debug.perf_cnt_width)

template2('hdl/rvex/core/core_pkg.vhd.tpl', 'hdl/rvex/core/core_pkg.vhd', **data)

# Configure rvex.h.
template2('cgen/templates/rvex.h.tpl', 'cgen/templates/rvex.h', creg_base = '0x%08X' % cfg.memory.ctrl_base_rvex)

# Configure pipeline.ini.
if cfg.special.extend_pipeline:
    misconfigured('Extended pipeline is not yet supported!')

done('rvex is enabled')
