This directory contains all VHDL sources needed by the r-VEX that can be shared
between multiple core types. It needs to be compiled into library 'rvex'.

bus     - Contains VHDL sources to do with bus arbitration and multiplexing.

common  - Packages and sources common to all other units.

gaisler - Contains interfaces between the rvex processor files and an AMBA bus.
          Requires sources from grlib.

periph  - Basic peripherals to connect to the bus.

utils   - Utility libraries which are not specific to the rvex processor, but
          are used extensively throughout the rest of the sources.

