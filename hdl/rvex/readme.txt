This directory contains all VHDL sources for the core-specific library. In
systems with just one core type you can just compile these sources in the
shared 'rvex' library; in heterogeneous systems you need to compile these
sources in a different library for each core.

cache   - Contains the sources for the reconfigurable instruction and data
          cache for the rvex processor.

core    - Contains the sources for the rvex processor itself.

system  - Contains system files which wrap the rvex processor core and add
          caches or local on-chip memories to it.

